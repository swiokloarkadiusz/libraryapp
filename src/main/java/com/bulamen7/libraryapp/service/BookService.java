package com.bulamen7.libraryapp.service;

import com.bulamen7.libraryapp.exception.NotFoundException;
import com.bulamen7.libraryapp.model.AuthorDto;
import com.bulamen7.libraryapp.model.BookDto;
import com.bulamen7.libraryapp.model.BookDto.CreateBookCommand;
import com.bulamen7.libraryapp.model.BookDto.UpdateBookCommand;
import com.bulamen7.libraryapp.repository.AuthorRepository;
import com.bulamen7.libraryapp.repository.BookRepository;
import com.bulamen7.libraryapp.repository.entity.AuthorEntity;
import com.bulamen7.libraryapp.repository.entity.BookEntity;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Service
@AllArgsConstructor
@Slf4j
public class BookService {
    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;

    //todo: remove optional
    public void createBook(CreateBookCommand command) {
        log.info("creating book. Command: {}", command);
        Set<AuthorEntity> authors = getCollect(command);
        if (bookRepository.existsByTitle(command.getTitle())) {
            throw new IllegalStateException("Book with given title already exists: " + command.getTitle());
        }
        log.debug("authors: {}", authors);
        BookEntity book = new BookEntity(command.getTitle(), command.getIsbn(), command.getReleaseYear(), command.isAvailable(), command.getPrice(), authors);
        bookRepository.save(book);
    }

    private Set<AuthorEntity> getCollect(CreateBookCommand command) {
        if (command.getAuthorIds() == null) {
            return Set.of();
        }

        Set<AuthorEntity> authors = new HashSet<>();
        for (Long id : command.getAuthorIds()) {
            Optional<AuthorEntity> author = authorRepository.findById(id);
            if (author.isEmpty()) {
                throw new IllegalStateException("There is no author with id: " + id);
            }
            authors.add(author.get());
        }
        return authors;
    }

    public Set<BookDto> findAllBooks() {
        log.info("Getting all books");
        return bookRepository.findAllBooks().stream().map(this::mapTo).collect(toSet());
    }

    public BookDto findBookById(Long id) {
        log.info("FindBookById {}", id);
        return bookRepository.findById(id).map(this::mapTo).orElseThrow(() -> new NotFoundException(id, "Book"));
    }

    private BookDto mapTo(BookEntity book) {
        log.info("Mapping entity to dto");
        return new BookDto(book.getTitle(), book.getIsbn(), book.getReleaseYear(), book.getPrice(), map(book.getAuthors()));
    }

    public void removeBookById(Long id) {
        log.info("Removed Book with id {}", id);
        bookRepository.deleteById(id);
    }

    public void putUpdate(Long id, UpdateBookCommand updateBook) {
        log.info("PutUpdate method");
        BookEntity entity = bookRepository.findById(id).orElseThrow(() -> new RuntimeException("Book cant be update"));

        entity.setTitle(updateBook.getTitle());
        entity.setIsbn(updateBook.getIsbn());
        entity.setReleaseYear(updateBook.getReleaseYear());
        entity.setAvailable(false);
        entity.setPrice(updateBook.getPrice());

        //TODO: fix book authors

        if (updateBook.getAuthors() != null) {
            Set<AuthorEntity> authorEntities = new HashSet<>();
            for (Long authorId : updateBook.getAuthors()) {
                AuthorEntity byId = authorRepository.findById(authorId).get();
                authorEntities.add(byId);
            }
            entity.setAuthors(authorEntities);
        } else {
            entity.setAuthors(null);
        }
        bookRepository.save(entity);
    }

    private Set<AuthorDto> map(Set<AuthorEntity> entities) {
        return entities.stream()
                .map(this::map)
                .collect(toSet());
    }

    private AuthorDto map(AuthorEntity author) {
        return new AuthorDto(author.getId(), author.getName(), author.getLastname(), null);
    }
}


