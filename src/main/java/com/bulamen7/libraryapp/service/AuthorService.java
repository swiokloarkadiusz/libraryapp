package com.bulamen7.libraryapp.service;

import com.bulamen7.libraryapp.exception.NotFoundException;
import com.bulamen7.libraryapp.model.AuthorDto;
import com.bulamen7.libraryapp.model.AuthorDto.CreateAuthorCommand;
import com.bulamen7.libraryapp.model.AuthorDto.UpdateAuthorCommand;
import com.bulamen7.libraryapp.model.BookDto;
import com.bulamen7.libraryapp.repository.AuthorRepository;
import com.bulamen7.libraryapp.repository.BookRepository;
import com.bulamen7.libraryapp.repository.entity.AuthorEntity;
import com.bulamen7.libraryapp.repository.entity.BookEntity;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Service
@Slf4j
@AllArgsConstructor
public class AuthorService {
    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;

    public void createAuthor(CreateAuthorCommand command) {
        log.info("Creating author. Command {}", command);
        Set<BookEntity> books = getCollect(command);
        AuthorEntity author = new AuthorEntity(command.getName(), command.getLastname(), books);
        authorRepository.save(author);
    }

    private Set<BookEntity> getCollect(CreateAuthorCommand command) {
        if (command.getBookIds() == null) {
            return Set.of();
        }

        Set<BookEntity> books = new HashSet<>();
        for (Long id : command.getBookIds()) {
            Optional<BookEntity> book = bookRepository.findById(id);
            if (book.isEmpty()) {
                throw new IllegalStateException("There is no author with id: " + id);
            }
            books.add(book.get());
        }
        return books;
    }


    public Set<AuthorDto> findAllAuthors() {
        log.info("Getting all authors");

        return authorRepository.findAllAuthors()
                .stream()
                .map(this::mapTo)
                .collect(toSet());
    }

    private AuthorDto mapTo(AuthorEntity author) {
        log.info("Mapping entity to dto");
        return new AuthorDto(author.getId(), author.getName(), author.getLastname(), mapBooks(author.getBooks()));

    }

    public AuthorDto findAuthorById(Long id) {
        log.info("FindAuthorById {} ", id);
        return authorRepository.findById(id)
                .map(this::mapTo)
                .orElseThrow(() -> new NotFoundException(id, "Author"));
    }

    public void removeAuthorById(Long id) {
        log.info("Removed Author with id {}", id);
        authorRepository.deleteById(id);
    }

    public void putUpdate(Long id, UpdateAuthorCommand updateAuthor) {
        log.info("PutUpdate method");
        AuthorEntity entity = authorRepository
                .findById(id)
                .orElseThrow(() -> new RuntimeException("Author cant be update"));

        if (updateAuthor.getName() != null) {
            entity.setName(updateAuthor.getName());
        }
        if (updateAuthor.getLastname() != null) {
            entity.setLastname(updateAuthor.getLastname());
        }
        Set<BookEntity> bookEntitiesEntities = new HashSet<>();
        for (Long bookId : updateAuthor.getBookIds()) {
            BookEntity byId = bookRepository.findById(bookId).get();
            bookEntitiesEntities.add(byId);
        }
        entity.setBooks(bookEntitiesEntities);
        authorRepository.save(entity);

        authorRepository.save(entity);
    }


    private Set<BookDto> mapBooks(Set<BookEntity> entities) {
        return entities.stream()
                .map(this::map)
                .collect(toSet());
    }

    private BookDto map(BookEntity book) {
        return new BookDto(book.getTitle(), book.getIsbn(), book.getReleaseYear(), book.getPrice(), null);
    }
}




















