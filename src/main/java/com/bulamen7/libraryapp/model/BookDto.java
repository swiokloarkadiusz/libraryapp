package com.bulamen7.libraryapp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.math.BigDecimal;
import java.util.Set;

@Value
public class BookDto {
    String title;
    String isbn;
    int releaseYear;
    BigDecimal price;
    @JsonIgnoreProperties(value = "books")
     Set<AuthorDto> authors;

    @Value
    @AllArgsConstructor
    public static class CreateBookCommand {
        String title;
        String isbn;
        int releaseYear;
        boolean available;
        BigDecimal price;
        Set<Long> authorIds;
    }

    @Value
    @AllArgsConstructor
    public static class UpdateBookCommand {
        String title;
        String isbn;
        int releaseYear;
        boolean available;
        BigDecimal price;
        Set<Long> authors;
    }
}
