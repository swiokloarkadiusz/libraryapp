package com.bulamen7.libraryapp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Setter
@Getter
public class AuthorDto {
    private Long id;
    private String name;
    private String lastname;
    @JsonIgnoreProperties(value = "authors")
    private Set<BookDto> books;


    @Getter
    @AllArgsConstructor
    @ToString
    public static class CreateAuthorCommand {
        private String name;
        private String lastname;
        private Set<Long> bookIds;
    }

    @Getter
    @AllArgsConstructor
    @ToString
    public static class UpdateAuthorCommand {

        private String name;
        private String lastname;
        private Set<Long> bookIds;
    }
}
