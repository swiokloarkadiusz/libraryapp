package com.bulamen7.libraryapp.controller;

import com.bulamen7.libraryapp.model.AuthorDto;
import com.bulamen7.libraryapp.model.AuthorDto.CreateAuthorCommand;
import com.bulamen7.libraryapp.model.AuthorDto.UpdateAuthorCommand;
import com.bulamen7.libraryapp.service.AuthorService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/authors")
@AllArgsConstructor

public class AuthorController {
    private final AuthorService authorService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createAuthor(@RequestBody CreateAuthorCommand command) {
        authorService.createAuthor(command);
    }

    @GetMapping
    public ResponseEntity<Set<AuthorDto>> getAuthors() {
        Set<AuthorDto> authors = authorService.findAllAuthors();
        return new ResponseEntity<>(authors, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AuthorDto> getAuthorById(@PathVariable Long id) {
        return new ResponseEntity<>(authorService.findAuthorById(id), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void updateAuthor(@PathVariable Long id, @RequestBody UpdateAuthorCommand command) {
        authorService.putUpdate(id, command);

    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        if (authorService.findAuthorById(id) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        authorService.removeAuthorById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
