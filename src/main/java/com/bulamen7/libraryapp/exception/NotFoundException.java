package com.bulamen7.libraryapp.exception;


public class NotFoundException extends RuntimeException {
    private static final String EXCEPTION_MESSAGE = "There is no id %s with type %s";

    public NotFoundException(long id, String type) {
        super(String.format(EXCEPTION_MESSAGE, id, type));
    }
}
