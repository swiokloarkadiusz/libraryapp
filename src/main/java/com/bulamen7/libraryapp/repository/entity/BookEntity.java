package com.bulamen7.libraryapp.repository.entity;

import com.bulamen7.libraryapp.repository.db.BaseEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "books")
@Getter
@Setter
@NoArgsConstructor
@Builder
@ToString(exclude = "authors")
public class BookEntity extends BaseEntity {

    private String title;
    private String isbn;
    private int releaseYear;
    private boolean available;
    private BigDecimal price;
    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(name = "books_authors", joinColumns = {@JoinColumn(name = "book_id")},
            inverseJoinColumns = {@JoinColumn(name = "author_id")})
    private Set<AuthorEntity> authors = new HashSet<>();

    public BookEntity(String title, String isbn, int releaseYear, boolean available, BigDecimal price, Set<AuthorEntity> authors) {
        this.title = title;
        this.isbn = isbn;
        this.releaseYear = releaseYear;
        this.available = available;
        this.price = price;
        this.authors = authors;
    }

    public void addAuthor(AuthorEntity author) {
        this.authors.add(author);
        author.getBooks().add(this);
    }

    public void removeAuthor(AuthorEntity author) {
        this.authors.remove(author);
        author.getBooks().remove(this);
    }
}