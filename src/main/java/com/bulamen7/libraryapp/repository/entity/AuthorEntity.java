package com.bulamen7.libraryapp.repository.entity;

import com.bulamen7.libraryapp.repository.db.BaseEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "authors")
@Getter
@Setter
@NoArgsConstructor
@ToString(exclude = "books")
@Builder
public class AuthorEntity extends BaseEntity {

    private String name;
    private String lastname;

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(name = "books_authors", joinColumns = {@JoinColumn(name = "author_id")},
            inverseJoinColumns = {@JoinColumn(name = "book_id")})
    private Set<BookEntity> books = new HashSet<>();

    public AuthorEntity(String name, String lastname, Set<BookEntity> books) {
        this.name = name;
        this.lastname = lastname;
        this.books = books;
    }

    public void addBook(BookEntity book) {
        this.books.add(book);
        book.getAuthors().add(this);
    }

    public void removeBook(BookEntity book) {
        this.books.remove(book);
        book.getAuthors().remove(this);
    }
}