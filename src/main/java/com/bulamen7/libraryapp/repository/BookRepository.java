package com.bulamen7.libraryapp.repository;

import com.bulamen7.libraryapp.repository.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface BookRepository extends JpaRepository<BookEntity, Long> {
    BookEntity findBookEntityByTitle(String name);

    @Query("from books b left join fetch b.authors")
    Set<BookEntity> findAllBooks();

    boolean existsByTitle(String title);

}
