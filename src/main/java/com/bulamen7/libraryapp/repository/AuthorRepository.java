package com.bulamen7.libraryapp.repository;

import com.bulamen7.libraryapp.repository.entity.AuthorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface AuthorRepository extends JpaRepository<AuthorEntity, Long> {
    @Query("from authors a left join fetch a.books")
    Set<AuthorEntity> findAllAuthors();
}
